# Currently we use Zalando PG operator for k8s, in case when we will use RDS
# don't forget add pg_endpoint to the argocd helm chart
#module "rds" {
#  source  = "terraform-aws-modules/rds/aws"
#  version = "3.5.0"
#
#
#  identifier = "alpha-db"
#
#  engine               = "postgres"
#  engine_version       = "12.11"
#  family               = "postgres12.11" # DB parameter group
#  major_engine_version = "12"         # DB option group
#  instance_class       = var.pg_settings.instance_class
#
#  allocated_storage     = var.pg_settings.allocated_storage
#  max_allocated_storage = var.pg_settings.max_allocated_storage
#  storage_encrypted     = false
#
#  name     = var.pg_cred.username
#  username = var.pg_cred.username
#  password = var.pg_cred.password
#  port     = var.pg_settings.port
#
#  multi_az               = false
#  subnet_ids             = [ aws_subnet.eks-private-subnet-1.id, aws_subnet.eks-private-subnet-2.id ]
#  vpc_security_group_ids = [ aws_security_group.allow_pg.id ]
#
#  backup_retention_period = 0
#  skip_final_snapshot     = true
#  deletion_protection     = false
#
#  performance_insights_enabled          = false
#  create_monitoring_role                = false
#
#  parameters = [
#    {
#      name  = "autovacuum"
#      value = 1
#    },
#    {
#      name  = "client_encoding"
#      value = "utf8"
#    }
#  ]
#
#  db_option_group_tags = {
#    "Environment" = "staging"
#  }
#}
