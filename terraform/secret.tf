resource "kubernetes_secret" "appcenter-repo-secret" {
  depends_on = [helm_release.argocd]
  metadata {
    name      = "appcenter-repo-secret"
    namespace = var.argocd_namespace_name
  }
  data = {
    username = var.alpha_cred.username
    password = var.alpha_cred.password
  }
}
