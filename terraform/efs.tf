resource "aws_efs_file_system" "efs" {
  depends_on = [aws_vpc.main-eks-vpc]
  creation_token = "eks-alpha"

  tags = {
    Name = "eks-alpha"
  }
}

resource "aws_efs_mount_target" "efs-mount-eks-subnet-1" {
  file_system_id  = aws_efs_file_system.efs.id
  subnet_id       = aws_subnet.eks-private-subnet-1.id
  security_groups = [aws_security_group.efs-eks-sg.id]
}

resource "aws_efs_mount_target" "efs-mount-eks-subnet-2" {
  file_system_id  = aws_efs_file_system.efs.id
  subnet_id       = aws_subnet.eks-private-subnet-2.id
  security_groups = [aws_security_group.efs-eks-sg.id]
}

resource "aws_security_group" "efs-eks-sg" {
  name        = "efs-eks-sg"
  description = "Allow EFS traffic"
  vpc_id      = aws_vpc.main-eks-vpc.id

  ingress {
    description      = "Allow EFS traffic"
    from_port        = 2049
    to_port          = 2049
    protocol         = "tcp"
    cidr_blocks      = ["192.168.0.0/16"]
  }

  egress {
    from_port        = 2049
    to_port          = 2049
    protocol         = "tcp"
    cidr_blocks      = ["192.168.0.0/16"]
  }

  tags = {
    Name = "Allow EFS traffic"
  }
}
