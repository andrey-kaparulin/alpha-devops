variable "cluster_name" {
  default = "eks-alpha"
}

variable "cluster_version" {
  default = "1.21"
}

variable "region" {
  default = "eu-central-1"
}

variable "argocd_namespace_name" {
  default = "argocd"
}

variable "argocd_password" {
  default = "ARGOCD_PASSWORD"
}

locals {
  full_cluster_name = "${terraform.workspace}-${var.cluster_name}"

  common_tags = {
    Team      = "alpha"
    Terraform = true
  }

  cluster_autoscale_service_account_namespace = "kube-system"
  cluster_autoscale_service_account_name      = "cluster-autoscaler"
}

variable "alpha_cred" {
  description = "Credentials with access to alpha repositories"
  type = object({
    username = string
    password = string
  })

  default = {
    username : "ALPHA_USERNAME"
    password : "ALPHA_PASSWORD"
  }
}

variable "pg_cred" {
  description = "Credentials for PG"
  type = object({
    username = string
    password = string
  })

  default = {
    username : "PG_USERNAME"
    password : "PG_PASSWORD"
  }
}


variable "pg_settings" {
  description = "Settings for PG"
  type = object({
    allocated_storage     = number
    max_allocated_storage = number
    port                  = number
    instance_class        = string
  })

  default = {
    allocated_storage : 20
    max_allocated_storage : 50
    port : 5432
    instance_class : "db.m5d.xlarge"
  }
}
