resource "aws_internet_gateway" "main-eks-gw" {
  vpc_id = aws_vpc.main-eks-vpc.id

  tags = {
    Name = "main-eks-gw"
  }
}
