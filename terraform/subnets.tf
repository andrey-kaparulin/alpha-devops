resource "aws_subnet" "eks-public-subnet-1" {
  # cidr end by 192.168.63.255
  cidr_block = "192.168.0.0/18"
  vpc_id     = aws_vpc.main-eks-vpc.id

  availability_zone = "eu-central-1a"
  # required for EKS
  map_public_ip_on_launch = true

  tags = {
    Name                                        = "eks-public-subnet-1"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                    = 1
  }
}

resource "aws_subnet" "eks-public-subnet-2" {
  cidr_block = "192.168.64.0/18"
  vpc_id     = aws_vpc.main-eks-vpc.id

  availability_zone = "eu-central-1b"
  # required for EKS
  map_public_ip_on_launch = true

  tags = {
    Name                                        = "eks-public-subnet-2"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                    = 1
  }
}

resource "aws_subnet" "eks-private-subnet-1" {
  cidr_block = "192.168.128.0/18"
  vpc_id     = aws_vpc.main-eks-vpc.id

  availability_zone = "eu-central-1a"

  tags = {
    Name                                        = "eks-private-subnet-1"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"           = 1
  }
}

resource "aws_subnet" "eks-private-subnet-2" {
  cidr_block = "192.168.192.0/18"
  vpc_id     = aws_vpc.main-eks-vpc.id

  availability_zone = "eu-central-1b"

  tags = {
    Name                                        = "eks-private-subnet-2"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"           = 1
  }
}
