resource "aws_nat_gateway" "eks-private-nat-gw-1" {
  allocation_id = aws_eip.eks-private-nat-eip-1.id
  subnet_id     = aws_subnet.eks-public-subnet-1.id

  tags = {
    Name = "eks-private-nat-gw-1"
  }
}

resource "aws_nat_gateway" "eks-private-nat-gw-2" {
  allocation_id = aws_eip.eks-private-nat-eip-2.id
  subnet_id     = aws_subnet.eks-public-subnet-2.id

  tags = {
    Name = "eks-private-nat-gw-2"
  }
}
