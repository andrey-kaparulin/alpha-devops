resource "helm_release" "argocd" {
  provider         = helm.eks
  name             = "argocd"
  repository       = "https://argoproj.github.io/argo-helm"
  chart            = "argo-cd"
  version          = "3.2.2"
  depends_on       = [module.eks, aws_efs_mount_target.efs-mount-eks-subnet-1, aws_efs_mount_target.efs-mount-eks-subnet-2]
  namespace        = var.argocd_namespace_name
  create_namespace = true
# add this value to the argocd values, in case when we use RDS PG, currently K8S Zalando
#  pg_endpoint   = "postgresql-postgresql",
  values = [
    "${templatefile("argocd/argocd-values.yaml",
      {
        region        = var.region,
        clusterName   = module.eks.cluster_id,
        alb_ctrl_arn  = module.iam_assumable_role_with_oidc.iam_role_arn,
        oidc_role_arn = module.iam_assumable_role_with_oidc_aws_secret_reader.iam_role_arn
    })}"
  ]

  set_sensitive {
    name  = "configs.secret.argocdServerAdminPassword"
    value = bcrypt(var.argocd_password)
  }
  set {
    name  = "configs.secret.argocdServerAdminPasswordMtime"
    value = timeadd(timestamp(), "-24h")
  }
}

data "kubectl_file_documents" "crds" {
  content = file("aws-lb-ctrl/crds.yaml")
}

resource "kubectl_manifest" "aws_lb_controller_crds" {
  count     = length(data.kubectl_file_documents.crds.documents)
  yaml_body = element(data.kubectl_file_documents.crds.documents, count.index)
}
