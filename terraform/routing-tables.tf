resource "aws_route_table" "eks-public-rt" {
  vpc_id = aws_vpc.main-eks-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main-eks-gw.id
  }

  tags = {
    Name = "eks-public-rt"
  }
}

resource "aws_route_table" "eks-private-rt-1" {
  vpc_id = aws_vpc.main-eks-vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.eks-private-nat-gw-1.id
  }

  tags = {
    Name = "eks-private-rt-1"
  }
}

resource "aws_route_table" "eks-private-rt-2" {
  vpc_id = aws_vpc.main-eks-vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.eks-private-nat-gw-2.id
  }

  tags = {
    Name = "eks-private-rt-2"
  }
}
