module "eks" {
  version = "18.2.0"

  source = "terraform-aws-modules/eks/aws"

  cluster_name    = var.cluster_name
  cluster_version = var.cluster_version

  vpc_id = aws_vpc.main-eks-vpc.id
  subnet_ids = [
    aws_subnet.eks-private-subnet-1.id,
    aws_subnet.eks-private-subnet-2.id
  ]

  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true

  # Managed Node Groups
  eks_managed_node_groups = {
    frontend = {
      desired_size = 1
      max_size     = 2
      min_size     = 1

      instance_types = ["t3.large"]
      labels = {
        Environment = "dev"
      }
    }
  }

  # node will be deployed only inside private VPC subnet, so we can use 0.0.0.0/0 rule
  node_security_group_additional_rules = {
    efs_ingress = {
      description = "NFS ingress"
      protocol    = "TCP"
      from_port   = 2049
      to_port     = 2049
      type        = "ingress"
      self        = true
    }
    efs_egress  = {
      description = "NFS egress"
      protocol    = "TCP"
      from_port   = 2049
      to_port     = 2049
      type        = "egress"
      cidr_blocks = ["0.0.0.0/0"]
    }
    pg_ingress = {
      description = "PG ingress"
      protocol    = "TCP"
      from_port   = 5432
      to_port     = 5432
      type        = "ingress"
      self        = true
    }
    pg_egress = {
      description = "PG egress"
      protocol    = "TCP"
      from_port   = 5432
      to_port     = 5432
      type        = "egress"
      cidr_blocks = ["0.0.0.0/0"]
    }
    ingress_allow_access_from_control_plane_to_webhook = {
      type                          = "ingress"
      protocol                      = "tcp"
      from_port                     = 9443
      to_port                       = 9443
      source_cluster_security_group = true
      description                   = "Allow access from control plane to webhook port of AWS load balancer controller"
    }
  }

  tags = {
    Name = "eks-alpha"
  }
}

# Security configuration

# This data will be used, by helm provider
data "aws_eks_cluster" "eks" {
  name = module.eks.cluster_id
}
data "aws_eks_cluster_auth" "eks" {
  name = module.eks.cluster_id
}

# tls certificate of eks
data "tls_certificate" "cluster" {
  url = module.eks.cluster_oidc_issuer_url
}

resource "aws_iam_openid_connect_provider" "cluster" {
  url = module.eks.cluster_oidc_issuer_url
  client_id_list = [
    "sts.amazonaws.com",
  ]
  thumbprint_list = [
    data.tls_certificate.cluster.certificates[0].sha1_fingerprint
  ]
}
