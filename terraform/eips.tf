resource "aws_eip" "eks-private-nat-eip-1" {
  depends_on = [aws_internet_gateway.main-eks-gw]

  tags = {
    Name = "eks-private-nat-eip-1"
  }
}

resource "aws_eip" "eks-private-nat-eip-2" {
  depends_on = [aws_internet_gateway.main-eks-gw]

  tags = {
    Name = "eks-private-nat-eip-2"
  }
}
