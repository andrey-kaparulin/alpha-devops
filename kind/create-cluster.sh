#!/bin/bash
# 1 master, 2 workers
# Use custom config to make ingress-nginx work
# From https://kind.sigs.k8s.io/docs/user/ingress/

cat <<EOF | kind create cluster --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
  extraPortMappings:
  - containerPort: 80
    hostPort: 80
    protocol: TCP
  - containerPort: 443
    hostPort: 443
    protocol: TCP
  - containerPort: 22
    hostPort: 32222
    protocol: TCP
- role: worker
  extraMounts:
  - hostPath: ../kind-persistent-share
    containerPath: /opt/kind-persistent-share
    readOnly: false
- role: worker
  extraMounts:
  - hostPath: ../kind-persistent-share
    containerPath: /opt/kind-persistent-share
    readOnly: false
EOF


echo "Deploying ingress-nginx..."
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
echo "Applying Kibana dashboard configmap"
kubectl apply -f ./common/kibana-dashboards/config-map-response-status.yaml

echo -e "\n\n\ Wait 20s for Ingress deployment, we will use it in next steps \n\n\n"
sleep 20
echo "Done"
