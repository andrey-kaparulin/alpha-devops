# Purpose of this Makefile - k8s local dev env
# Expected you have: Kind, helm, helmfile (don't forget install helm-diff plugin)
#		helm plugin install https://github.com/databus23/helm-diff

build-alpha-backend:
	docker build --platform linux/amd64 \
		-t alpha-backend:dev \
		-f ../alpha-backend/Dockerfile ../alpha-backend/

# since env vars should be passed on "npm build stage"
# and there is no way pass it for k8s directly, we use ARG
build-alpha-frontend:
	docker build --platform linux/amd64 \
		--build-arg REACT_APP_API_HOST=alpha-backend.local \
		--build-arg REACT_APP_API_PORT=80 \
		-t alpha-frontend:dev \
		-f ../alpha-frontend/Dockerfile ../alpha-frontend/

remove-alpha-backend:
	docker container exec -it kind-control-plane crictl rmi docker.io/library/alpha-backend:dev

remove-alpha-frontend:
	docker container exec -it kind-control-plane crictl rmi docker.io/library/alpha-frontend:dev

load-alpha-backend:
	kind load docker-image alpha-backend:dev

load-alpha-frontend:
	kind load docker-image alpha-frontend:dev

helm-repo-init:
	helm repo add bitnami https://charts.bitnami.com/bitnami
	helm repo update

helmfile-delete-apply:
	helmfile -f ./helmfile/helmfile.yaml delete
	helmfile -f ./helmfile/helmfile.yaml apply

kind-create-cluster:
	./kind/create-cluster.sh

kind-delete-cluster:
	./kind/delete-cluster.sh

kind-redeploy-cluster: kind-delete-cluster kind-create-cluster


create-local-env: kind-redeploy-cluster \
				  build-local-env

build-local-env: build-alpha-frontend \
				 build-alpha-backend \
				 load-alpha-frontend \
				 load-alpha-backend \
				 helmfile-delete-apply

rebuild-local-env: remove-alpha-frontend \
				   remove-alpha-backend \
				   build-local-env \
				   helmfile-delete-apply

recreate-local-env: remove-alpha-frontend \
					remove-alpha-backend \
					create-local-env

remove-local-env: remove-alpha-frontend \
				  remove-alpha-backend \
				  kind-delete-cluster
