apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: kube-prometheus-stack
  annotations:
    argocd.argoproj.io/sync-wave: "10"
  namespace: {{ .Values.namespace }}
  finalizers:
    - resources-finalizer.argocd.argoproj.io
spec:
  destination:
    namespace: {{ .Values.project_namespace.kube_prometheus_stack }}
    name: {{ .Values.spec.destination.name }}
  project: {{ .Values.project }}
  source:
    chart: kube-prometheus-stack
    repoURL: https://prometheus-community.github.io/helm-charts
    targetRevision: '39.4.0'
    helm:
      # https://github.com/argoproj/argo-cd/issues/8128#issuecomment-1163989690
      # skip crd, we will install them in next app
      skipCrds: true
      version: v3
      values: |
        prometheus:
          ingress:
            enabled: true
            ingressClassName: nginx
            hosts:
              - alpha-prometheus.local
            paths:
              - /
            pathType: Prefix
          prometheusSpec:
            nodeSelector:
              node: dedicated
            podMonitorSelectorNilUsesHelmValues: false
            serviceMonitorSelectorNilUsesHelmValues: false
            storageSpec:
              volumeClaimTemplate:
                spec:
                  storageClassName: longhorn
                  accessModes: ["ReadWriteOnce"]
                  resources:
                    requests:
                      storage: 2Gi
        prometheusOperator:
          nodeSelector:
            node: dedicated
          tls:
            enabled: false

        alertmanager:
          alertmanagerSpec:
            nodeSelector:
              node: dedicated
          ingress:
            enabled: true
            ingressClassName: nginx
            hosts:
              - alpha-alertmanager.local
            paths:
              - /
            pathType: Prefix

        grafana:
          ingress:
            enabled: true
            ingressClassName: nginx
            hosts:
              - alpha-grafana.local
            paths:
              - /
            pathType: Prefix
          additionalDataSources:
          - name: loki
            access: proxy
            basicAuth: false
            basicAuthPassword:
            basicAuthUser:
            editable: true
            jsonData:
                tlsSkipVerify: true
            orgId: 1
            type: loki
            url: http://loki:3100
            version: 1
        additionalPrometheusRules:
        {{- range .Values.prometheusRules }}
        - {{ printf "prometheus-rules/%s.yaml" . | $.Files.Get | indent 10  | trim }}
    {{- end }}
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
    syncOptions:
      - CreateNamespace=true
